import React, { useState } from 'react';
import "./Styles.css";
import FbSvg from '../../assets/icons/Footer/2.svg';
import InstaSvg from '../../assets/icons/Footer/3.svg';
import LinkedInSvg from '../../assets/icons/Footer/4.svg';
import EmailSvg from '../../assets/icons/Footer/5.svg';

const Footer = () => { 
  return (
    <div className="footer-container ">
        <div className='footer-services container'>
          <div className='row inside-row'>
              <div className="footer-row col-4 col-sm-2">
                <h6>Products</h6>
                <ul>
                  <li>Open Banking</li>
                  <li>Health Care Management</li>
                </ul>
              </div>

              <div className="footer-row col-4 col-sm-2">
                <h6>Solutions</h6>
                  <ul>
                    <li>Helth Care</li>
                    <li>Financet</li>
                    <li>API Managenemnt</li>
                    <li>IoT</li>
                    <li>Cloud</li>
                  </ul>
                </div>
                
              <div className="footer-row col-4 col-sm-2">
                <h6>Company</h6>
                <ul>
                    <li>About</li>
                    <li>Team</li>
                    <li>Careers</li>
                    <li>Events</li>
                </ul>
              </div>

              <div className="footer-row col-4 col-sm-3">
                <h6>Resources</h6>
                <ul>
                    <li>Documentation</li>
                    <li>Blogs</li>
                    <li>Case Studues</li>
                    <li>Articles</li>
                </ul>
              </div>
            </div>
          </div>
          
        <div>
            <img className='icon-container' src={FbSvg} alt={'facebook'} />
            <img className='icon-container' src={InstaSvg} alt={'insta'} />
            <img className='icon-container' src={LinkedInSvg} alt={'linkedin'} />
            <img className='icon-container' src={EmailSvg} alt={'Email'} />
        </div>
        <hr style={{backgroundColor:'#FAFAFA',height:'0.2px',width:'50%'}}/>
        <div className='copyright'>
            <p>Copyright &#169; 2020 Prioritix Co. All right reserved</p>
        </div>  
    </div>
  );
}

export default Footer;