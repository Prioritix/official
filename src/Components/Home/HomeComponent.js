import React from 'react';
import Homejumbotron from './Homejumbotron';
import ServicesComponent from './ServicesComponent';
import HomeIntro from './HomeIntro';
import WhoWeAre from './WhoWeAreComponent';
import ContactForm from './ContactForm';
import WordsClients from './WordsClients';
import SocialMedia from '../SocialMedia';
import CustomerChat from './CustomerChat';


function Home(){
    return(
        <React.Fragment>
                
            <Homejumbotron/> 
            <SocialMedia/>
            <CustomerChat/>
            <ServicesComponent />
            <HomeIntro/>
            <WhoWeAre />
            <WordsClients/>
            <ContactForm/>    
            
        </React.Fragment>
    )
}

export default Home;