import React from 'react'
import './Form.css'
// import FormControl from '@material-ui/core/FormControl';
// import Input from '@material-ui/core/Input';
// import InputLabel from '@material-ui/core/InputLabel';
// import FormHelperText from '@material-ui/core/FormHelperText';
import { Col, Row, Button, Form, FormGroup, Label, Input } from 'reactstrap';
export default function ContactForm() {
    return (
        <div className='form-container '>
            <div className="shadow rounded" style={{padding:'10px 30px'}}>
                <div className='form-title'>
                    <h4>Let's Discuss Your Ideas</h4>
                </div>
                <div className='form-body '>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="name">Name</Label>
                                    <Input type="text" name="name" id="name" placeholder="What is your name?" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="email">Email</Label>
                                    <Input type="email" name="email" id="email" placeholder="Email" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Label for="address">Address</Label>
                            <Input type="text" name="address" id="exampleAddress" placeholder="1234 Main St"/>
                        </FormGroup>
                        <FormGroup>
                            <Label for="message">Message</Label>
                            <Input type="textarea" name="message" id="message"  placeholder="your ideas"/>
                        </FormGroup>
                        <Button>Send Message</Button>
                    </Form>
                </div>
            </div>    
        </div>
    )
}