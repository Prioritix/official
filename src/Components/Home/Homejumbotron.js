import React from 'react';
import { Jumbotron, } from 'reactstrap';
import './HomeJumbotron.css'
import {Button} from '@material-ui/core';
import {Link} from 'react-router-dom';
import AnimationText from './Animation';

export default function Homejumbotron() {
    return (
        <div className='jumbotron jumbotron-fluid'>
            <div className='header-text' >
                <div className='jumbo-header'>
                    <div className='container ' >
                        <div className='row'>
                            <div className='col-11 col-sm-10 col-md-8 col-lg-9 mx-auto header-card' >
                                <div className='text-container  rounded' style={{textAlign:'center'}} > 
                                    <div  style={{padding:'20px'}}>
                                        <h2 style={{letterSpacing:'0.2rem'}} >BESPROKE WEB SOLUTIONS</h2>
                                        <hr style={{height:'0.1px',backgroundColor:'rgb(176,179,177,0.8)'}}/>
                                        <h5 style={{marginTop:'1rem',letterSpacing:'1rem'}}>PRIORITIX </h5>
                                        <p style={{letterSpacing:'0.1rem'}}>Build and launch your product with an eperienced and reliable team</p>
                                        <div>
                                            <AnimationText/>    
                                        </div>
                                        <Link to='/contactus' className='m-1'>
                                        <Button variant='contained' color='primary' size='small'>Learn More</Button>
                                        </Link>
                                        <Link to='/contactus' className='m-1'>
                                            <Button variant='contained' color='secondary' size='small'>CONTACT US</Button>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    )
}
