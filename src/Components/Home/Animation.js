import * as React from "react";

import { useDencrypt } from "use-dencrypt-effect";

const values = ["Responsive Web Development", "Cross Platform App Development", "Software Development", "Proffesional Content Writting"];

const options= {
    chars: [" "]
  }

const AnimationText = () => {
  const { result, dencrypt } = useDencrypt(options);

  React.useEffect(() => {
    let i = 0;

    const action = setInterval(() => {
      dencrypt(values[i]);

      i = i === values.length - 1 ? 0 : i + 1;
    }, 3500);

    return () => clearInterval(action);
  }, []);

  return <div style={{fontSize:'25px',margin:'20px',color:'#4FCAB7',height:'70px'}}>{result}</div>;
};

export default AnimationText;