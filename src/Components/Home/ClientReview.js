import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';


const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

export default function ClientReview() {
  const classes = useStyles();

  return (
    <Card className=''>
       <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
           <img style ={{height:'3rem'}} src='https://cdn.britannica.com/s:800x450,c:crop/43/172743-138-545C299D/overview-Barack-Obama.jpg'/>
          </Avatar>
        }
        action={
          <IconButton aria-label="settings">
            
          </IconButton>
        }
        title="Shrimp and Chorizo Paella"
        subheader="September 14, 2016"
      />
      <CardActionArea>
        <CardContent>
          <Typography className={classes.pos} color="textSecondary">
          "They're very willing to assemble the team that we ask for if we have certain preferences."
        </Typography>
        </CardContent>
      </CardActionArea> 
    </Card>
  );
}