import React from 'react'
import './HomeIntro.css'
import Button from '@material-ui/core/Button';

export default function HomeIntro() {
    return (
        <div className=' container HomeIntro-main-container' >
            <div className='row' >
                <div className='HomeIntro-text-container col-12 col-md-6'>
                    <div className = 'm-3'>
                        <div className='row ' style={{color:'#F1A518',textTransform:'uppercase'}}>
                            <h4><strong>Online Success Starts with</strong></h4>
                        </div>
                        <div className='row'>
                            <h5>Web Design And Development</h5>
                        </div>
                        <div className='row'>
                            <p className='def'>We understand that a premium web solution is always a result of an outstanding partnership. We go beyond the regular call of duty and invest a lot in cultivating trust and transparency with all of our clients. Your success is our success.</p>
                        </div>
                        <div className='row'>
                            <Button style={{marginTop:'1rem'}} variant="contained" color="primary" href="/">
                                Read More
                            </Button>
                        </div>    
                    </div>
                </div>
                <div className='HomeIntro-img-container col-12 col-md-6'>
                        <div className=' HomeIntro-image'></div>
                </div>    
            </div>
        </div>
    )
}
