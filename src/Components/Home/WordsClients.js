import React from 'react';
import ClientCarosal from './ClientCarosal';
import "./WordClient.css"

export default function WordsClients() {
    return (
        <div className='words-clients'>
            <div className='client-carosal'>
                <div style={{textAlign:'center'}}>
                    <h2><span style={{color:'orange'}}>Great</span> Reviews</h2>
                </div>
                <div className="m-2" style={{textAlign:'center'}}>
                    <p>97 Out Of 100 Clients Have Given Us A Five Star Rating On Google & Clutch</p>
                </div>
                <div className='mx-3'>
                    <ClientCarosal/>
                </div>    
            </div>
        </div>
    )
}
