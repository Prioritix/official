import React from 'react'
import './WhoWeAre.css'
import Button from '@material-ui/core/Button';

export default function WhoWeAre() {
    return (
        <div className= 'whoweare-main-container'>
            <div className='container'>
                <div className = 'row'>
                    <div className = 'col-md-6 order-md-12' style={{marginTop:'50px'}}>
                        <div className='row 'style={{color:'#F1A518',textTransform:'uppercase'}}>
                            <h4><strong>who we are</strong></h4>
                        </div>
                        <div className='row '>
                            <h5>A Young and Ambitionus Startup with an Eye for Success </h5> 
                        </div>
                        <div className='def-who-we-are row'>
                            We are a group of talented individuals with great ambition and determination to deliver a world class product to our customers, by providing the best quality work at an extremely reasonable price with timely delivery.  
                        </div>
                        <div className='def-who-we-are row'>
                            <Button style={{marginTop:'1rem'}} variant="contained" color="primary" href="/">
                                Read More
                            </Button>
                        </div>    
                    </div>
                    <div className = ' col-md-6 order-md-1  WhoWeAre-image' >  
                    </div>
                </div>
            </div>
        </div>
    )
}
