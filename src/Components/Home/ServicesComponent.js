import React from 'react';
import './ServicesComponent.css';
import ServiceCarousel from './ServiceCarouselCompinent';

export default function ServicesComponent() {
    return (
        <div className='main-container'>
            <div className='first-container'>
                <div className='container'>
                    <div className='row our-services-topic'>
                        <h2><span style={{color:'#4D5056'}}>What We </span ><span style ={{color:'orange'}}>Offer</span></h2>
                    </div>
                    <div className='row services-intro'>
                        A broad set of capabilities offered under one roof makes us a versatile partner to meet all your digital ambitions
                    </div>
                    <div >
                        <div className='col-12' >
                            <ServiceCarousel />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
