import React from 'react';
import MessengerCustomerChat from 'react-messenger-customer-chat';

export default function CustomerChat(){
  return(
    <div>
        <MessengerCustomerChat
            pageId="<PAGE_ID>"
            appId="<APP_ID>"
            htmlRef="<REF_STRING>"
        />
    </div>
      

  )
}
  
