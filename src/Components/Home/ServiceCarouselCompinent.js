import React from'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ServiceCard from './ServiceCard';
import  './ServiceCarosalDiv.css';
 
const responsive = {
  superLargeDesktop: {
    // the naming can be any, depends on you.
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 4
  },
  tablet: {
    breakpoint: { max: 1024, min: 600 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 600, min: 0 },
    items: 1
  }
};
function ServiceCarousel(){
    return(
        <div className="service-div">
            <Carousel responsive={responsive}>
              <div className='m-1'><ServiceCard title='Responsive Web Development' image='https://www.417marketing.com/wp-content/uploads/2013/03/Desktop-tablet-and-mobile-phone-on-desk-all-displaying-the-same-architecture-firms-website-an-example-of-responsive-web-design-2-scaled.jpg'/></div>
              <div className='m-1'><ServiceCard title='Cross Platform App Development' image='https://svitla.com/uploads_converted/0/972-mobile.webp?1530014255'/></div>
              <div className='m-1'><ServiceCard title='Software Development' image ='https://www.onlinecoursereport.com/wp-content/uploads/2020/04/OCR-SoftwareDevelopment-25-1280x707.jpg'/></div>
              <div className='m-1'><ServiceCard title='Proffesional Content Writting' image='https://www.homebizwave.com/wp-content/uploads/2017/12/Advantages-of-content-marketing.jpg'/></div>
            </Carousel>
        </div>
    )
}

export default ServiceCarousel;
