import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import React from 'react';
import ClientReview from "./ClientReview";

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 5
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3
  },
  tablet: {
    breakpoint: { max: 1024, min: 600 },
    items: 2
  },
  mobile: {
    breakpoint: { max: 600, min: 0 },
    items: 1
  }
};

export default function ClientCarosal() {
    return (
        <div>
            <Carousel responsive={responsive}>
              <div className='m-1'><ClientReview/></div>
              <div className='m-1'><ClientReview /></div>
              <div className='m-1'><ClientReview/></div>
              <div className='m-1'><ClientReview /></div>
              <div className='m-1'><ClientReview /></div>
              <div className='m-1'><ClientReview /></div>
            </Carousel>
        </div>
    )
}
