import React from 'react';
import "./SocialMedia.css";
import WhatsAppIcon from '@material-ui/icons/WhatsApp';
import PhoneIcon from '@material-ui/icons/Phone';
import EmailIcon from '@material-ui/icons/Email';

export default function SocialMedia() {
    return (
        <div>
            <div class="icon-bar">
                <a href="#" className="whatsapp"><WhatsAppIcon/></a> 
                <a href="#" className="contact"><PhoneIcon/></a> 
                <a href="#" className="email"><EmailIcon/></a> 
            </div>
        </div>
    )
}
