import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText
} from 'reactstrap';
import './NavbarComponent.css';
import {Button} from '@material-ui/core'
import {Link} from 'react-router-dom';

const PrioritixNavBar = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div >
      <Navbar color='dark' dark expand="md" fixed='top'>
        <NavbarBrand ><Link to='/home'><span style={{letterSpacing:'0.2rem'}}>PRIORITIX</span></Link></NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto " navbar>
            <NavItem>
                <NavLink><Link to="/home">Home</Link></NavLink>
            </NavItem>
            <NavItem>
                <NavLink><Link to="/aboutus">About Us</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="/contactus">Contact Us</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="#">Services</Link></NavLink>
            </NavItem>
            <NavItem>
              <NavLink><Link to="#">Careers</Link></NavLink>
            </NavItem>  
          </Nav>
          <Link to='/quote'><Button color='secondary' variant='outlined'>REQUEST QUOTE</Button></Link>
        </Collapse>
      </Navbar>
    </div>
  );
}

export default PrioritixNavBar;