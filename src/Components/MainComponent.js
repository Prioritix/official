import React from 'react';
import PrioritixNavBar from './NavbarComponent';
import Footer from './Footer/Footer';
import Home from './Home/HomeComponent';
import {Switch, Route,Redirect, HashRouter} from 'react-router-dom';
import Contactus from './Contact/ContactComponent';
import Aboutus from './About/AboutComponent';
import SocialMedia from './SocialMedia';


function Main(){
    return(
        <div>
            <PrioritixNavBar />
                <Switch>
                    <Route  path='/home' exact component={Home}/>
                    <Route exact path='/contactus' component={Contactus}/>
                    <Route exact path='/aboutus' component={Aboutus}/>
                    <Route path={'/*'} exact component={Home} />
                </Switch>
            <Footer/>
        </div>
    )
}

export default Main;